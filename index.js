const http = require("http");



let database = [
    {
        "Welcome to booking System" 
    },
    {
        "Welcome to your profile",
        "Here's our courses available"
    }
]

http.createServer((req,res)=>{
    if(req.url === "/users" && req.method === "GET"){
        res.writeHead(200,{"Content-Type": "_application/json"});
        res.write(JSON.stringify(database));
        res.end();
    }
    if (req.url === "/users" && req.method === "POST"){
        let requestBody = ""
        req.on("data", function(data){
            requestBody += data
        })
        req.on("end", function(){
            requestBody = JSON.parse(requestBody)

            let newUser = {
                "name": requestBody.name,
                "email": requestBody.email
            }
            database.push(newUser);
            console.log(database);

            res.writeHead(200, {"Content-Type": "_application/json"});
            res.write(JSON.stringify(newUser));
            res.end();
        })
    }
}).listen(4000)

console.log("Server is running at localhost:4000");